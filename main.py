import smtplib
from email.message import EmailMessage
from pathlib import Path
from string import Template


def create_email(sender, recipient, subject, content, email_type='text'):
    msg = EmailMessage()
    msg['From'] = sender
    msg['To'] = recipient
    msg['Subject'] = subject
    if email_type == 'text':
        msg.set_content(content)
    else:
        msg.set_content(content, email_type)
    return msg


def create_html_content(name, date):
    html_template = Template(Path('templates/email_template.html').read_text())
    return html_template.substitute(name=name, date=date)


def send_email(msg):
    with smtplib.SMTP(host='127.0.0.1', port=2525) as smtp_server:
        smtp_server.ehlo()
        # smtp_server.starttls()
        # smtp_server.login('username', 'password')
        smtp_server.send_message(msg)
        print('Email sent!')


if __name__ == '__main__':
    my_email = create_email('Edu', 'testaaja@test.com', 'Hello from Python', 'Hey! This is a test message')
    send_email(my_email)

    html_content = create_html_content('Janna', 'tomorrow')
    html_email = create_email('Edu', 'Janna@mail.com', 'Go out', html_content, email_type='html')
    send_email(html_email)
