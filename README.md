# Chapter 60: Sending emails

## Running smtp4dev SMTP server in a Docker

Project have `docker-compose.yml` file, which includes configurations to run `smtp4dev` locally in a Docker.

To run it, execute following command:
```shell
docker compose up -d 
```

This will start SMTP server with web interface.
Web interface can be accessed at this URL [http://127.0.0.1:3000](http://127.0.0.1:3000).

## Run program which sends an email

To run the Python script, which will send an email execute following command:
```shell
python main.py
```

New message should appear in smtp4dev web interface.
